from unittest.mock import ANY
from urllib.parse import quote

import pytest
import requests

BASE_URL = 'http://localhost:8080/1'


def create_messages(json):
    response = requests.post(
        f'{BASE_URL}/messages',
        json=json,
    )

    assert response.status_code == 200


def get_messages(recipient, sender=None, limit=None, days=None):
    response = requests.get(
        f'{BASE_URL}/messages/{quote(recipient)}',
        params={'sender': sender, 'limit': limit, 'days': days}
    )

    assert response.status_code == 200
    return response.json()


def test_messages():
    # Create a bunch of messages
    for x in range(236):
        if x % 2 == 0:
            create_messages({
                "message": f"Hello there! How are you? {x}",
                "recipient": "John",
                "sender": "Jane",
            })
            create_messages({
                "message": f"{x} Brawndo?",
                "recipient": "Jane",
                "sender": "Mike",
            })
        else:
            create_messages({
                "message": f"Good, you? {x}",
                "recipient": "Jane",
                "sender": "John",
            })
            create_messages({
                "message": f"{x} Brawndo's got what plants crave",
                "recipient": "Mike",
                "sender": "Jane",
            })

    # Get all the messages where John was the recipient
    messages = get_messages("John")
    assert len(messages) == 118
    for x in range(len(messages)):
        assert messages[x] == {
                "message": f"Hello there! How are you? {x*2}",
                "recipient": "John",
                "sender": "Jane",
                "timestamp": ANY
            }

    # Get the messages where John was the recipient limiting to 100 messages
    messages = get_messages("John", limit=100)
    assert len(messages) == 100
    for x in range(len(messages)):
        assert messages[x] == {
            "message": f"Hello there! How are you? {x*2}",
            "recipient": "John",
            "sender": "Jane",
            "timestamp": ANY
        }

    messages = get_messages("John", days=30)
    assert len(messages) == 118
    for x in range(len(messages)):
        assert messages[x] == {
            "message": f"Hello there! How are you? {x * 2}",
            "recipient": "John",
            "sender": "Jane",
            "timestamp": ANY
        }

    messages = get_messages("Mike", sender='Jane', limit=100)
    assert len(messages) == 100
    for x in range(len(messages)):
        assert messages[x] == {
            "message": f"{1+x*2} Brawndo's got what plants crave",
            "recipient": "Mike",
            "sender": "Jane",
            "timestamp": ANY
        }

    messages = get_messages("Mike", sender='Jane', days=30)
    assert len(messages) == 118
    for x in range(len(messages)):
        assert messages[x] == {
            "message": f"{1+x*2} Brawndo's got what plants crave",
            "recipient": "Mike",
            "sender": "Jane",
            "timestamp": ANY
        }
