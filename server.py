import connexion
import logging

logging.basicConfig(level=logging.INFO)

app = connexion.App(__name__, specification_dir='app/openapi/')
app.add_api('messenger.yml', base_path='/1')


logging.info('Running openapi spec on http://0.0.0.0:8080/1/ui')

app.run(port=8080)
