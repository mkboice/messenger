from datetime import datetime
from typing import Optional

from app.messenger.data import UserConversations, Conversations, Message

# In memory storage of messages. In the real world this should be replaced with a database
USER_CONVERSATIONS = UserConversations()
CONVERSATIONS = Conversations()


def post_message(body):

    message = Message(body.get('sender'), body.get('recipient'), body.get('message'))
    message.generate_timestamp()
    # Get convo_id
    convo_id = USER_CONVERSATIONS.get_conversation(message.sender, message.recipient)
    # Append message to convo_id
    CONVERSATIONS.add_message(convo_id, message)
    # return something
    return message


def get_messages(recipient: str, sender: Optional[str] = None, limit: int = None, days: int = None):
    # TODO: Should the messages be in ascending or descending order? Query parameter for that maybe?
    #  Clients could use it differently

    if sender:
        # Get convo_id from user_1 and user_2
        convo_id = USER_CONVERSATIONS.get_conversation(recipient, sender)
        messages = CONVERSATIONS.get_messages(convo_id, recipient)
        # Filter messages based on query parameters 100 or 30 days
    else:
        # Get all messages
        convo_ids = USER_CONVERSATIONS.get_all_conversations(recipient)
        messages = CONVERSATIONS.get_all_messages(convo_ids, recipient)

    if limit:
        # Need to sort the messages based on timestamp when getting messages from multiple conversations. Otherwise
        # since lists maintain the order it should be ok
        # TODO: Probably makes more sense to put the sorting into the get_all_messages method
        sorted_messages = sorted(messages, key=lambda message: message.timestamp)
        return sorted_messages[:int(limit)]

    if days:
        current_time = datetime.utcnow()
        output = []
        # Need to sort the messages based on timestamp when getting messages from multiple conversations. Otherwise
        # since lists maintain the order it should be ok
        # TODO: Probably makes more sense to put the sorting into the get_all_messages method
        sorted_messages = sorted(messages, key=lambda message: message.timestamp)
        for message in sorted_messages:
            days_old = current_time - message.timestamp
            if days_old.days < days:
                output.append(message)
        return output

    return messages
