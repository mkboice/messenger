from dataclasses import dataclass
import logging
from datetime import datetime
from typing import Optional


@dataclass
class Message:
    sender: str
    recipient: str
    message: str
    timestamp: datetime = None

    def generate_timestamp(self):
        self.timestamp = datetime.utcnow()


class UserConversations:
    """
    User Conversations where primary key is user1 and secondary is user2 and value is the convo_id
    For example:
    user_conversations = {
        'user_1': {
            'user_2': 'convo_1',
            'user_3': 'convo_2'
        },
        'user_2': {
            'user_1': 'convo_1',
            'user_3': 'convo_3'
        }
    }
    """
    def __init__(self):
        self._data: dict = {}
        self.next_id: int = 0

    def _get_next_id(self):
        id = self.next_id
        self.next_id = self.next_id + 1
        return id

    def get_conversation(self, user_1, user_2) -> str:
        # TODO: Could sort the users to halve the amount of data to store
        try:
            return self._data[user_1][user_2]
        except KeyError as e:  # Could easily be other exceptions to track here
            logging.info(f'Conversation not found between {user_1} and {user_2}')
            convo_id = self._get_next_id()
            if user_1 not in self._data.keys():
                self._data[user_1] = {}
            self._data[user_1][user_2] = convo_id
            if user_2 not in self._data.keys():
                self._data[user_2] = {}
            self._data[user_2][user_1] = convo_id
            return convo_id

    def get_all_conversations(self, recipient: str):
        logging.info(f'UserConversations: {self._data}')
        return [id for _, id in self._data[recipient].items()]


class Conversations:
    """
    To avoid duplicating the message data since that will be the largest part, save the conversation with an id
    conversation {[convo_id] = [{messages}]
    conversations = {
        'convo_1': [
            {
                'sender': '',
                'recipient': '',
                'message': '',
                'timestamp': ''
            },
            {
                'sender': '',
                'recipient': '',
                'message': '',
                'timestamp': ''
            }
        ]
    }
    """

    def __init__(self):
        self._data = {}

    def get_messages(self, convo_id: int, recipient: Optional[str] = None) -> [Message]:
        try:
            if recipient:
                return [message for message in self._data[convo_id] if message.recipient == recipient]
            else:
                return self._data[convo_id]
        except KeyError as e:
            logging.info(f'No conversation with {convo_id}. Returning []')
            return []

    def get_all_messages(self, convo_ids: [int], recipient: Optional[str] = None):
        messages = []
        for convo_id in convo_ids:
            if recipient:
                messages.extend([message for message in self._data[convo_id] if message.recipient == recipient])
            else:
                messages.extend(self._data[convo_id])
        return messages

    def add_message(self, convo_id, message: Message):
        # TODO: Could add some message clean up here if more than 100 messages AND older than 30 days
        if convo_id not in self._data:
            self._data[convo_id] = []
        self._data[convo_id].append(message)





# GET /users/?name=''

# POST /messages
# GET /messages/
# GET /messages/<message_id>
# GET /conversations/
# GET /conversations/<conversation_id>
#
