# Simple Messenger API

## High Level Requirements
Build an api that supports a web-application to enable two users to send
short text messages to each other, like Facebook Messages app or Google
Chat.
Ignore Authorization, Authentication and Registration. Assume this is a
global api that can be leveraged by anyone, security is not a concern.
1. A short text message can be sent from one user (the sender) to another
(the recipient).
2. Recent messages can be requested for a recipient from a specific sender - with a limit of 100 messages or all 
messages in last 30 days.
3. Recent messages can be requested from all senders - with a limit of 100 messages or all messages in last 30 days.
4. Document your api like you would be presenting to a web team for use.
5. Show us how you would test your api.
6. Ensure we can start / invoke your api.

## Assumptions
1. Requirements #2 and #3 are looking for only received messages, not both sides of the conversation

## Design Decisions
* Used the string user name in the path instead of a userid since the requirements says to ignore registration. The string user
name in the path isn't ideal but the better replacement is the userid that would be created at registration time and could
be indexed via the user's email and return a userid. With that in place you could have users change their name, or have duplicate
names of users, etc. Out of scope for this project.

## Run
```
# Build the docker container
# cd to directory
docker build . -t messenger-mkboice

# Run the unit tests and coverage
docker run --rm -it messenger-mkboice pytest

# Run the api server
docker run --rm -it -p 8080:8080 messenger-mkboice
```

## Next Steps
* Add `GET /conversations/` and `GET /conversations/<conversation_id>` endpoints to organize the messages
