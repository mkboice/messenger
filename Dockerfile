FROM python:3.9-alpine

WORKDIR /usr/src/app

COPY requirements.txt  ./
COPY requirements-test.txt  ./
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir -r requirements-test.txt

COPY . .

CMD [ "python", "server.py" ]
